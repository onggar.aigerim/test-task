import styled from "styled-components";

const Div = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 74px 0 99px;
  padding: 0 10px;
  @media screen and (max-width: 980px) { 
    flex-direction: column;
    align-items: center;
  }
  & .ad_card {
    max-width: 399px;
    height: 547px;
    object-fit: cover;
    @media screen and (max-width: 980px) {
      margin-bottom: 20px;
    }
  }
  & .ad_card_img {
    display: block;
    width: 100%;
    height: 414px;
    object-fit: cover;
  }
  & .ad_card_text {
    font-weight: 700;
    font-size: 14px;
    line-height: 19px;
    padding: 55px 0 55px 94px;
    box-shadow: 0px 4px 59px -14px rgba(171, 124, 0, 0.29);
    background: url(../images/details.png) 25px 55% no-repeat;
  }
`;

const Advertising = () => {
  return <>
    <Div>
      <div className="ad_card price">
        <img className="ad_card_img"  src="uploads/stairs.jpg" alt="Stairs" />
        <p className="ad_card_text">
          Same great quality. New lower prices.
        </p>
      </div>
      <div className="ad_card affordable">
        <img className="ad_card_img" src="uploads/living_room.jpg" alt="Living" />
        <p className="ad_card_text">
          Everyday Essentials, High quality affordable 
        </p>
      </div>
      <div className="ad_card journey">
        <img className="ad_card_img"  src="uploads/hall.jpg" alt="Hall" />
        <p className="ad_card_text">
          Join the makeover journey 
        </p>
      </div>
    </Div>
  </>
};

export default Advertising;