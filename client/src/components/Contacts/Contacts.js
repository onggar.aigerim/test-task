import styled from "styled-components";

const Div = styled.div`
  background: #FFF5DA;
  text-align: center;
  padding: 127px 0 104px;
  & .contact_us_title {
    font-weight: 600;
    font-size: 36px;
    line-height: 49px;
    margin-bottom: 15px;
  }  
  & .contact_us_subtitle {
    display: block;
    font-weight: 400;
    font-size: 29px;
    line-height: 40px;
    margin-bottom: 15px;
  }
  & .number {
  position: relative;
  max-width: 702px;
  margin: 0 auto;
  
    @media screen and (max-width: 720px) {
      display: flex;
      flex-direction: column;
      align-items: center;
    }
  }
  & .number_send, & .number_phone {
    font-weight: 400;
    font-size: 29px;
    line-height: 40px;
  }
  & .number_phone {
    width: 702px;
    padding: 15px 190px 15px 77px;
    background: url(../images/flag.png) 23px  no-repeat;
    @media screen and (max-width: 980px) {
      max-width: 702px;
    }
    @media screen and (max-width: 720px) {
      width: 100%;
      padding: 15px 10px 15px 70px;
    }
  }
  & .number_send {
    position: absolute;
    top: 7px;
    right: 23px;
    padding: 10px 40px;
    background: #ffc839;
    border: none;
    @media screen and (max-width: 720px) {
      position: relative;
      display: block;
      text-align: center;
    }
  }
`;

const Contacts = () => {
  return <>
    <Div>
      <h3 className="contact_us_title">
        Book professionals from your phone
        <small className="contact_us_subtitle">
          Enter your mobile number to get the professionals help
        </small>
      </h3>
      <form className="number">
        <input className="tel" class="number_phone" />
        <button className="submit" class="number_send">Send</button>
      </form>
    </Div>
  </>
};

export default Contacts;