import styled from "styled-components";

const Div = styled.div`
border: 1px solid;
border-radius: 8px 8px 0 0;
background: #effaff;
margin: 20px auto;
max-width: 700px;
& .list_title {
  background: #146a93;
  display: block;
  text-transform: uppercase;
  font-weight: 700;
  font-size: 26px;
  line-height: 24px;
  padding: 16px 24px;
  color: #fff;
  border-radius: 8px 8px 0 0;
}
`;

const Table = styled.table`
  padding: 20px;
  padding-top: 0;
  width: 100%;
  table-layout: fixed;

  & td {
    border-bottom: 1px solid khaki;
    line-height: 40px;
    font-size: 20px;
    padding: 10px;
    & input {
      display: block;
    }
    & button {
      vertical-align: top;
    }
  };

  & th {
    text-align: start;
    border-bottom: 2px solid khaki;
    line-height: 50px;
    font-size: 20px;
  };

  & .num {
    width: 5%;
  };

  & .delete {
    width: 10%;
    vertical-align: top;
  };
`;

const UserList = ({ usersList, deleteUser, updateUser }) => {
  return <Div> 
    <span className="list_title">User list</span>
    <Table>
      <thead>
        <tr>
          <th className="num">#</th>
          <th>Name</th>
          <th>Surname</th>
          <th>Age</th>
          <th className="delete">Delete</th>
        </tr>
      </thead>
      <tbody>
        {usersList.map((user, i) => {
          return <tr key={user._id}>
            <td>{i + 1}</td>
            <td>{user.name}</td>
            <td>{user.surname}</td>
            <td>{user.age} <span onClick={() => updateUser(user._id)}>&#9998;</span> </td>
            <td><button onClick={() => deleteUser(user._id)}>x</button></td>
          </tr>
        })}
      </tbody>
    </Table>
  </Div>
};

export default UserList;