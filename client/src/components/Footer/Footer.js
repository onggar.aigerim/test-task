import { NavLink } from "react-router-dom";
import styled from "styled-components";
import { MAIN_PAGE } from "../../constants";

const FooterContainer = styled.footer`
  background: #093a7a;
  color: #cacaca;
  padding: 10px;
  @media screen and (max-width: 980px) {
    padding: 53px;
  }
  @media screen and (max-width: 720px) {
    text-align: center;
  }

  & .footer_logo {
    display: block;
    width: 110px;
    height: 16px;
    background: url(../images/logo.svg) no-repeat;
  }
  & .footer {
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-top: 10px;
    padding: 0 10px;
    @media screen and (max-width: 980px) {
      flex-direction: column;
    }
    & .footer_text {
      font-size: 11px;
      line-height: 13px;
      @media screen and (max-width: 980px) {
        padding: 10px 0;
      }
    }
  }
      
  .google_play, .app_store {
    display: block;
    width: 134px;
    height: 46px;
    margin: 10px 0;
  }
  .google_play {
    background: url(../images/google_play.png) no-repeat;
  }

  .app_store {
    background: url(../images/app_store.png) no-repeat;
  }

`;

const Footer = () => {
  return <>
    <FooterContainer>
      <div className="container">
        <div className="footer">
          <NavLink className="footer_logo" to={MAIN_PAGE}></NavLink>
          <span className="footer_text">© 2021 Housesale Technologies Ltd.</span>
          <span className="footer_text">1760  Dancing Dove Lane, New York, USA</span>
          <span className="footer_text">sales@housesales.com</span>
          <span className="google_play"></span>
          <span className="app_store"></span>
        </div>
      </div>
    </FooterContainer>
  </>
};

export default Footer;