import Backdrop from "../Backdrop/Backdrop";
import loaderIcon from "../../../assets/img/826.png";
import "./Loader.css";

const Loader = ({loading}) => {
  return loading ? <>
    <Backdrop show={true}/>
    <img src={loaderIcon} className="Loader" alt="loader" />
  </> : null
};

export default Loader;