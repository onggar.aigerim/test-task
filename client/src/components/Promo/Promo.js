import styled from "styled-components";

const Div = styled.div`
  background: linear-gradient(0.87deg, rgba(2, 55, 135, 0.9) 0.69%, rgba(81, 38, 174, 0.4736) 100.48%, rgba(38, 92, 174, 0.0102906) 100.49%, rgba(195, 195, 196, 0.00520833) 100.5%, #76A6EF 100.51%), url(../uploads/promo_bg.jpg) no-repeat;
  background-size: cover;
  color: #fff;
  & .promo {
    padding: 202px 10px 283px;
    & .promo_title {
      max-width: 789px;
      font-weight: 700;
      font-size: 60px;
      line-height: 82px;
      @media screen and (max-width: 720px) {
        font-size: 40px;
        line-height: 60px;
      }
    }
    & .promo_subtitle {
      display: block;
      margin-top: 21px;
      font-weight: 400;
      font-size: 29px;
      line-height: 40px;
      @media screen and (max-width: 720px) {
        font-size: 20px;
        line-height: 30px;
      }
    }
  }
`;

const Promo = () => {
  return <>
    <Div className="promo_block">
      <div className="promo container">
        <h1 className="promo_title">
          Quality home services, on demand
          <small className="promo_subtitle">
            Experienced, hand-picked Professionals to serve you at your doorstep
          </small>
        </h1>
      </div>
    </Div>
  </>
};

export default Promo;