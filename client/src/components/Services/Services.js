import styled from "styled-components";

const Div = styled.div`
  display: grid;
  padding: 0 10px;
  grid-template-areas: 'serv_title serv_title serv_img' 'serv_items serv_items serv_img' 'serv_items serv_items serv_img';
  grid-template-columns: 1fr 3fr 3fr;
  margin-bottom: 98px;
  @media screen and (max-width: 980px) {
    grid-template-areas: 'serv_title serv_title serv_title' 'serv_img serv_img serv_img' 'serv_items serv_items serv_items';
    grid-template-columns: 3fr;
    justify-items: center;
  }
  & .services_title {
    grid-area: serv_title;
    font-weight: 600;
    font-size: 36px;
    line-height: 49px;
    @media screen and (max-width: 980px) {
      padding-bottom: 20px;
      text-align: center;
    }
  }
  & .services_items {
    list-style: none;
    grid-area: serv_items;
  }
  & .services_img {
    display: flex;
    align-items: center;
    width: 498px;
    height: 555px;
    grid-area: serv_img;
    background: url(../uploads/roof.jpg) 75px 0 no-repeat;
    background-size: contain;
    margin-left: 20px;
    @media screen and (max-width: 720px) {
      display: none
    }
  }
  & .services_img_bg {
    width: 75px;
    height: 431px;
    background: #ffc839;
    top: 30px;
  }
  & .service_item {
    padding: 17px 0 17px 117px;
  }
  & .service_item_title {
    font-weight: 400;
    font-size: 29px;
    line-height: 40px;
  }
  & .service_item_subtitle {
    display: block;
    font-size: 21px;
    line-height: 29px;
  }

  & .gpamophone {
    background: url(../images/gramophone.png) 0 50% no-repeat;
  }
  & .experts {
    background: url(../images/experts.png) 0 50% no-repeat;
  }
  & .equipped {
    background: url(../images/equipped.png) 0 50% no-repeat;
  }
`;

const Services = () => {
  return <>
    <Div>
      <h3 className="services_title">
        Why HOUSESALE Company?
      </h3>
      <ul className="services_items">
        <li className="service_item gpamophone">
          <h6 className="service_item_title">
            Transparent pricing
            <small className="service_item_subtitle">
              See fixed prices before you book. No hidden charges.
            </small>
          </h6>
        </li>
        <li className="service_item experts">
          <h6 className="service_item_title">
            Experts only
            <small className="service_item_subtitle">
              Our professionals are well trained and have on-job expertise.
            </small>
          </h6>
        </li>
        <li className="service_item equipped">
          <h6 className="service_item_title">
            Fully equipped
            <small className="service_item_subtitle">
              We bring everything needed to get the job done well.
            </small>
          </h6>
        </li>
      </ul>
      <div className="services_img">
        <div className="services_img_bg"></div>
      </div>
    </Div>
  </>
};

export default Services;