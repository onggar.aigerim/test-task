import styled from "styled-components";

const Form = styled.form`
  text-align: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin: 20px;

  & input, button {
  width: 200px;
  height: 30px;
  font-size: 20px;
  margin: 5px;
}
`;

const UsersForm = ({createUser, changHandler}) => {
  return <Form onSubmit={createUser}>
    <input
      type="text"
      name="name"
      placeholder="User name"
      onChange={changHandler}
    />
    <input
      type="text"
      name="surname"
      placeholder="User surname"
      onChange={changHandler}
    />
    <input
      type="number"
      name="age"
      placeholder="User age"
      onChange={changHandler}
    />
    <button> Add new User </button>
</Form>
};

export default UsersForm;