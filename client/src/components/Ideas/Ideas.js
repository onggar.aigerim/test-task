import styled from "styled-components";

const Div = styled.div`
  padding: 122px 10px;
  & .ideas_title {
    font-weight: 600;
    font-size: 36px;
    line-height: 49px;
    margin-bottom: 45px;
    @media screen and (max-width: 720px) {
      text-align: center;
    }
  }
  & .ideas_cards {
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    grid-gap: 17px;
    @media screen and (max-width: 980px) {
      grid-template-columns: 1fr 1fr;
    }
    @media screen and (max-width: 720px) {
      grid-template-columns: 1fr;
      justify-items: center;
    }
  }
  & .ideas_card_img {
    display: block;
    width: 100%;
    height: 406px;
    object-fit: cover;
  }
`;

const Ideas = () => {
  return <>
    <Div>
      <h4 className="ideas_title">
        More ideas and inspiration
      </h4>
      <div className="ideas_cards">
        <div className="ideas_card">
          <img src="uploads/attic.jpg" className="ideas_card_img" alt="" />
        </div>
        <div className="ideas_card">
          <img src="uploads/house.jpg" className="ideas_card_img" alt="" />
        </div>
        <div className="ideas_card">
          <img src="uploads/front_door.jpg" className="ideas_card_img" alt="" />
        </div>
        <div className="ideas_card">
          <img src="uploads/roof.jpg" className="ideas_card_img" alt="" />
        </div>
        <div className="ideas_card">
          <img src="uploads/house.jpg" className="ideas_card_img" alt="" />
        </div>
        <div className="ideas_card">
          <img src="uploads/roof.jpg" className="ideas_card_img" alt="" />
        </div>
        <div className="ideas_card">
          <img src="uploads/gray_house.jpg" className="ideas_card_img" alt="" />
        </div>
        <div className="ideas_card">
          <img src="uploads/house.jpg" className="ideas_card_img" alt="" />
        </div>
      </div>
    </Div>
  </>
};

export default Ideas;