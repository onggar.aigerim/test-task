import { HashRouter, Routes, Route } from 'react-router-dom';
import { MAIN_PAGE, MONGODB_PAGE } from './constants';
import Header from './containers/Header/Header';
import Main from "./containers/Main/Main";
import Users from "./containers/Users/Users";
import Footer from './components/Footer/Footer';


function App() {
  return (

    <>
     <HashRouter>
      <Header />
      <Routes>
        <Route path={MAIN_PAGE + "*"} element={<Main />} />
        <Route path={MONGODB_PAGE + "*"} element={<Users />} />
      </Routes>
      <Footer />
    </HashRouter>
      {/* <div className="App">
        <Main />
        <Users />
      </div> */}
    </>
    
  );
}

export default App;
