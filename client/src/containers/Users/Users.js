import { useState, useEffect } from "react";
import Axios from "axios";
import UsersForm from "../../components/UsersForm/UsersForm";
import UserList from "../../components/UsersList/UsersList";
import Loader from "../../components/UI/Loader/Loader";


const Users = () => {
  const [usersList, setUsersList] = useState([]);
  const [user, setUser] = useState({
    name: "",
    surname: "",
    age: 0
  })
  const [loading, setLoading] = useState(false);

  const changeHandler = (e) => {
    setUser({
      ...user,
      [e.target.name]: e.target.value
    });
    console.log(user);
  };

  const getUserList = async () => {
    await Axios.get("http://localhost:3001/getUsers").then((response) => {
      setUsersList(response.data);
    });
    setLoading(false);
  };

  useEffect(() => {
    getUserList();
  }, []);

  const createUser = async () => {
    setLoading(true);
    const name = user.name;
    const surname = user.surname;
    const age = user.age;
    await Axios.post("http://localhost:3001/createUser", {
      name,
      surname,
      age,
    }).then(() => {
      setUsersList([
        ...usersList,
        {
          name,
          surname,
          age,
        },
      ]);
      setLoading(false);
    });
  };

  const deleteUser = (id) => {
    setLoading(true);
    Axios.delete(`http://localhost:3001/delete/${id}`);
    getUserList();
  };

  const updateUser = (id) => {
    const newAge = prompt("Введите новую возраст: ");

    Axios.put("http://localhost:3001/update", {newAge: newAge, id: id})
      .then(() => {
        setUsersList(usersList.map((user) => {
          return user._id === id ? {_id: id, name: user.name, surname: user.surname, age: newAge} : user;
        }));
        getUserList();
      })
  }

  return (
    <div className="Users">
      <Loader loading={loading} />
      <UsersForm 
        createUser={createUser}
        changHandler={changeHandler}
      />
      <UserList
        usersList={usersList}
        deleteUser={deleteUser}
        updateUser={updateUser}
      />
    </div>
  );
}

export default Users;
