import styled from "styled-components";
import { NavLink } from "react-router-dom";
import { MAIN_PAGE } from "../../constants";
import { useState } from "react";

const Head = styled.header`
  font-family: 'Roboto', sans-serif;
  display: flex;
  justify-content: space-around;
  align-items: center;
  padding: 42px 10px;
  background: #093a7a;
  & .header_logo {
    display: block;
    width: 110px;
    height: 24px;
    background: url(../images/logo.svg) left center no-repeat;
  }
  & .header_menu {
    @media screen and (max-width: 720px) {
      display: none;
    };
    & .header_menu_link {
      font-size: 18px;
      line-height: 21px;
      color: #fff;
      margin-right: 42px;
      &:last-child {
        margin-right: 140px;
        @media screen and (max-width: 980px) {
          margin-right: 0;
        }
      }
      @media screen and (max-width: 720px) {
        margin-right: 0;
      }
    }
  }
  & .burger_menu {
    display: none;
    width: 44px;
    height: 27px;
    background: url(../images/menuBurger.svg) 50% 50% no-repeat;
    @media screen and (max-width: 720px) {
      display: block;
    }
  }

  & .show {
    display: block;
    @media screen and (max-width: 720px) {
      display: flex;
      flex-direction: column;
    };
  }

  & button {
    border: none;
  }
`;

const Header = () => {

  const [show, setShow] = useState(false);

  let menuClasses = ["header_menu"];

  if (show) {
    menuClasses.push("show");
  } else {
    menuClasses = ["header_menu"];
  }

  const burgerMenuClick = () => {
    setShow(!show);
  }

  return <>
    <Head>
      <NavLink className="header_logo" to={MAIN_PAGE}></NavLink>
      <nav className={menuClasses.join(" ")}>
        <NavLink className="header_menu_link" to={MAIN_PAGE}>Home</NavLink>
        <NavLink className="header_menu_link" to="mongodb/">MongoDB</NavLink>
      </nav>
      <button onClick={burgerMenuClick} className="burger_menu"></button>
    </Head>
  </>
};

export default Header;