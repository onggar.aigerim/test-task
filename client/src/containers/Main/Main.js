import Advertising from "../../components/Advertising/Advertising";
import Contacts from "../../components/Contacts/Contacts";
import Ideas from "../../components/Ideas/Ideas";
import Promo from "../../components/Promo/Promo";
import Services from "../../components/Services/Services";

const Main = () => {
  return <>    
    <Promo />
    <div class="container">
      <Advertising />
      <Services />
      <Contacts />
      <Ideas />
    </div>
  </>
};

export default Main;