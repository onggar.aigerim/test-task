const express = require("express");
const app = express();
const mongoose = require("mongoose");
const UserModel = require("./models/User");

const cors = require("cors");

app.use(express.json());
app.use(cors());

mongoose.connect(
  "mongodb+srv://aigerimonggar:unYDE3aMVQf7AY6@cluster0.edalh.mongodb.net/?retryWrites=true&w=majority"
);

app.get("/getUsers", (req, res) => {
  UserModel.find({}, (err, result) => {
    if (err) {
      res.json(err);
    } else {
      res.json(result);
    }
  });
});

app.post("/createUser", async (req, res) => {
  const user = req.body;
  const newUser = new UserModel(user);
  await newUser.save();

  res.json(user);
});


app.delete("/delete/:id", async (req, res) => {
  const id = req.params.id;
  
  await UserModel.findByIdAndRemove(id).exec();
  res.send("deleted")
});

app.put("/update", async (req, res) => {
  const newAge = req.body.newAge;
  const id = req.body.id;
  try {
    await UserModel.findById(id, (error, userToUpdate) => {
      userToUpdate.age = Number(newAge);
      userToUpdate.save();
    })
  } catch (e) {
    console.log(e);
  }

  res.send("updated");
})

app.listen(3001, () => {
  console.log("Server runs");
});
